package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int cols;
	private int rows;
	private CellState[][] array;

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	this.array = new CellState[rows][columns];
    	
    	for (int i = 0; i < rows; i++) {
    		for (int j = 0; j < columns; j++) {
    			array[i][j] = initialState;
    		}
    	}	
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() { 	
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
        	int x = row;
        	int y = column;
        	array[x][y] = element;
        }
        else {
        	throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
    	if (row >= 0 && row < numRows() && column >= 0 && column < numColumns()) {
    		return array[row][column];
        }
        else {
        	throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
    	CellGrid copiedGrid = new CellGrid(rows,cols,CellState.DEAD);
    	
    	for (int i = 0; i < rows; i++) {
    		for (int j = 0; j < cols; j++) {
    			CellState cs = get(i,j);
    			copiedGrid.set(i,j,cs);
    		}
    	}
    	
        return copiedGrid;
    }  
}
